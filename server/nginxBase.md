# 前言
> 本文章主要分两篇(上,下两篇)[因为这些都是要求作者的基础知识比较扎实，所以出的会比较久一点]
> * 上篇主要讲解nginx的基础搭建(如果有不知道web服务器的可以百度，或者我再做一篇图解web)
> * 下篇主要讲解nginx的三大特性的实际用途
>
> Nginx在互联网中作为一个不可或缺的组件，我们看到LNMP集成环境。LNMP（Linux+Nginx+Mysql+Php);
> 如果只知道这个不知道Nginx的搭建过程的话我们有nginx的主要特性会错过。比如nginx的优点
>>* 高可用(一个组件可以服务上千人)
>>* 热部署(可以做到不停机就能升级基础设施服务)
>>* 反向代理(通常可以，上游反向代理，下游负载均衡处理外部请求)

NGINX搭建
-------------
1. 版本 首先先下载一个
   + [Nginx社区版]('https://nginx.org')
   + [nginx企业版]('https://nginx.com') 两者没有太大区别，都支持主要几大功能。
   + 淘宝版本的Nginx对IO进行了优化，至今淘宝在使用的[Tengine]('http://tengine.taobao.org')。

2. nginx版本分支
    ![](../img/nginx.png)
   1. MainLine Version(主线版本开发)
   2. Stable(稳定版本)
   3. Legacy Version(历史版本)-- *如果项目需要上到选定版本可以选这个*
3. 下载（我们以Nginx Stable Version为例子)
   # 我们能看到Stable Version底下有三个栏目 #
   CHANGE-1.16 *是介绍这个版本和上个版本的区别*  
   nginx-1.16 pgp *这个是Linux版本下载选项，将会下载倒tar.gz*
   nginx/Windows1.16.1 *这个是Windows系统下载的,将会下载window包*
4. # Nginx文件目录(如下) #
   ![](../img/nginxDir.png)
   * conf(存放配置文件)
       * nginx.conf(主要的建站配置文件夹)
   - - -
   * contrib(是一些Linux的Vim配置文件)
        * 使用是将整个vim文件移动到~/.vim/profile目录底下，就会在vim下有高亮表现
   - - -
   * doc (documentation 文档目录)
        * 一些日志改变以及readme文档还有License授权
   * html(默认的html，网站的入口)
        * index.html 默认网页
        * 50x.html 常见的错误页面
   * logs(默认的日志文件夹)
   * temp(临时文件目录)a
## Windows下搭建
* windows下我们直接点击nginx根目录下的nginx.exe即可立即使用了 *闪退是正常现象，之后检查端口查看是否被占用，
 第一次启动推荐使用命令行启用*  
 ```.\nginxpath\nginx.exe```(一直停留不变则说明开启成功)  
 ![开启成功](../img/start.png)
 然后我们打开[http://localhost:80]('http://localhost')
 ![访问成功](../img/index.png)
 > 这样就是成功 如果失败了看下面

 ## 使用win + a启动命令行，拥有最高权限
 ```bash
netstart　-ano | findstr 80 #找到占用的pid号。然后将其
taskkill /F /pid  
```
 ![如下](../img/kill.png)
 > 在使用最开始的方法启动

## Linux环境下(Centos下)
>　Linux环境下使用最高权限执行(不考虑用户权限)
>* 首先先安装被依赖的插件
>> ```bash
>> yum install gcc gcc-c++ pcre pcre-devel zlib zlib-devel openssl openssl-devel -y
>> ```

 ![](../img/package.png)
```bash
tar -zxvf nginx-1.16.1.tar.gz
cd nginx-1.16.1
# 这下面几步骤是进行加载vim插件的，使其在修改conf的时候可以进行常亮
cd contrib
mv contrib ~/.vim
cd ..
# path自己需要安装的地方
./configure -prefix=/path/nginx
# configure完成后即可，make && make install去
# configure 是进行配置化操作
# make 是进行编译操作
# make install 是进行编译后文件的部署行为
# 为了方便就将make && make install 一起进行
```
![](../img/configure.png)
![](../img/make.png)
```bash
cd /nginxInStallPath
./nginx
#没有任何信息则是运行成功了。Linux的没有任何消息就是最好的消息
curl http://localhost
```
![](../img/curl.png)
搭建完成

```bash
./nginx -s reload #重新加载（Nginx能做到热部署的一个原因)
./nginx -s stop #关闭服务器
./nginx -t # 可以测试刚修改的nginx.conf能否生效
```
># 接下来这个是重头戏，是nginx的上限操作
```bash
# 下面是进行nginx.conf配置
cd conf
vim nginx.conf
```
> 首先是定义一个
```editorconfig
worker_processes number;
# error_log path #错误日志文件位置
#　error_log path notice 提醒错误文件位置
#　error_log path info 一些Logger::info的输出位置
events {
    work_connections 1024; #定义最大连接数量
}

# http 定义http的位置（高难度配置下章在讲解)
http {
    include mine.types; #定义包含类型,网页请求request_type的地方属性值设置地方在mine.types文件里卖你
    default_type：application/ocet-stream #设置属性流
    sendfile    on; #从一个 buffer到另一个 buffer的拷贝用于优化速度
    #tcp_nopush on;
    keepalive_timeout 60; #设置请求超时时间

    server {
        listen 80 # 设置监听的端口
        server_name localhost # 设置监听的域名
        charset koi8-r #设置字符集

        #access_log logs/host.access.log main #设置主访问日志存储

        location / { #设置正则表达式匹配localhost/底下的目录按照此位置进行处理，
                     #下篇文章配置php会用到
            root html; #设置网站根目录为html路径
            index index.html index.htm; #设置没有确定的资源文件下，会自动访问index.hmtl例如
                                        # 例如访问http://localhost会实际访问http://localhost/index.html
                                        # 或者 http://localhost/index.htm
           }
       error_page 404       /404.html #自定义错误页面代码下访问的页面

       error_page 500 502 503 504 /50x.html
       location = /50.html { #访问路径为50x.html 无论是500， 502， 503， 504， 505这些页面
            root html;
         }
        # location ~\.php$ {
        #   proxy_pass http://127.0.0.1;
        # }
        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

       #location ~/\.ht {
       #    deny all;拒绝所有用户访问
   　　 # }
    }
# 底下是配置https,https是现在还在比较流行的一种安全web服务器的基本要求
    https {
    }
}

```
# 总结
 > Nginx是现代互联网企业中不可缺少的一门技术，在微服务中也极为重要，尤其反向代理所带来的抗风险能力极为重要，热部署能力。
