#This Reposity is about my programmer
>* Maintenance 
>>  * Linux Operation(Because below list opera is in Linux)
>>  * nginx
>>      * [nginx上篇](server/nginxBase.md);
>>      * [nginx下篇](server/nginxMaster.md);
>>  * docker
>>  * tomcat
>>  * Liberty
>>  * Mixed(Nginx, docker and Liberty MiscoServer[share experience from Company Documentation])
>* Dev(Java)
>>  * Debugging experience
>>  * optimize experience
>* Data Analysic
>>  * PowerBI
>* Database
>>  * base
>>  * optimize
>>>   * index
>>>   * Data struction;

#这是我个人编程上的一些经验的仓库
>* 运维 
>>  * Linux(下面操作很多都是在linux上操作的)
>>  * nginx
>>  * docker
>>  * tomcat
>>  * Liberty
>>  * 多种技术混合(Nginx, docker and Liberty 负载均衡微服务[分享一些经验从公司文档和实战中])
>* 开发(Java)
>>  * 调试经验
>>  * 优化经验
>* 数据分析
>>  * PowerBI
>* 数据库
>>  * 基础操作
>>  * 优化
>>>   * 索引
>>>   * 数据结构