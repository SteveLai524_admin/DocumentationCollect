# Spring Security

## Spring Boot中的 Security的自动配置

* 启动Spring Security的默认配置， 并且创建一个servlet的过滤器并命名
  *springSecurityFilterChain*, 这个bean是一个用来相应所有安全在你应用(保护来源包括 保护应用的URLS还有验证提交用户名和密码，重定向登录表单等等)
* 创建一个*UserDetailsService*这个组件，并且提供一个用户名为user的用户和一个随机密码，该随机密码会在控制台打印出来
* 注册一个过滤器(Filter)装配位名为springSecurityFilterChain的bean在Servlet的容器中处理每个请求

### Spring Boot配置的不多，但是做了很多,功能总结

* 要求经过身份验证的用户才能与应用程序进行任何交互
* 生成一个默认的登录表单
* 让用户使用用户名user和密码登录到控制台以使用基于表单的身份验证进行身份验证(控制台中可以看到)
* 使用BCrypt保护密码存储
* 允许用户登出
* 预防CSRF攻击防范
* 会话固定保护
* Security Header请求集成
    * HTTP严格传输安全性用于安全请求
    * X-Content-Type-Options集成
    * 缓存控制（以后可以被您的应用程序覆盖以允许缓存您的静态资源）
    * X-XSS-保护集成
    * X-Frame-Options integration to help prevent Clickjacking
* 在以下Servlet中的API中集成功能
    * HttpServletRequest#getRemoteUser() 获取远程用户
    * HttpServletRequest.html#getUserPrincipal() 获取用户凭证
    * HttpServletRequest.html#isUserInRole(java.lang.String) 用户是否是属于这个角色
    * HttpServletRequest.html#login(java.lang.String, java.lang.String) 登录
    * HttpServletRequest.html#logout() 登出

## 回顾Filters

基于Servlet的应用程序中Spring Security的高级体系结构。 “身份验证，授权，防止利用漏洞”部分中建立了这种高级理解。

### Spring Security的Servlet支持基于Servlet过滤器，因此通常首先了解过滤器的作用会很有帮助。 下图显示了单个HTTP请求的处理程序的典型分层。

![](../img/securityfilter.png)

### FilterChain

客户端向应用程序发送请求，然后容器创建一个FilterChain， 其中包含应该根据请求URI的路径处理HttpServletRequest 的过滤器和Servlet。 在Spring MVC应用程序中，
Servlet是DispatcherServlet的实例。 一个Servlet最多只能处理一个HttpServletRequest和HttpServletResponse。 但是，可以使用多个过滤器来处理，也就编程一个过滤器链：

* 防止下游过滤器或Servlet被调用. 在这种情况下，过滤器通常将编写HttpServletResponse;
* 修改下游过滤器和Servlet使用的HttpServletRequest或HttpServletResponse;

*过滤器的功能来自传递给它的 传统的filter代码如下

```java
class SthFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        // do something before the rest of the application
        chain.doFilter(request, response); // invoke the rest of the application
        // do something after the rest of the application
    }
}
```

由于过滤器仅影响下游过滤器和Servlet，因此每个过滤器的调用顺序非常重要。

### DelegatingFilterProxy

Spring提供了一个名为DelegatingFilterProxy的Filter实现， 可以在Servlet容器的生命周期和Spring的ApplicationContext之间进行桥接。
Servlet容器允许使用其自己的标准注册Filters， 但是它不知道Spring定义的Bean。 DelegatingFilterProxy可以通过标准的Servlet容器机制进行注册，
但是可以将所有工作委托给实现Filter的Spring Bean。

![](../img/securityfilter1.png)

*DelegatingFilterProxy从ApplicationContext查找Bean Filter0，然后调用Bean Filter0。 DelegatingFilterProxy的伪代码可以在下面看到。*

```java
class DelegatingFilterProxy extends Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        // Lazily get Filter that was registered as a Spring Bean
        // For the example in DelegatingFilterProxy delegate is an instance of Bean Filter0
        Filter delegate = getFilterBean(someBeanName);
        // delegate work to the Spring Bean
        delegate.doFilter(request, response);
    }
}
```
### 延迟加载DelegatingFilterProxy的一些对象
*DelegatingFilterProxy的另一个好处是，
它可以延迟查找Filter bean实例。 
这很重要，因为容器需要在启动容器之前注册Filter实例。
但是，Spring通常使用ContextLoaderListener加载Spring Bean，
直到需要注册Filter实例之后才可以加载。*

##FilterChainProxy
Spring Security的Servlet支持包含在FilterChainProxy中。
FilterChainProxy是Spring Security提供的特殊过滤器，
允许通过SecurityFilterChain委派许多过滤器实例。 
由于FilterChainProxy是Bean，因此通常将其包装在DelegatingFilterProxy中。


![](../img/securityfilter2.png)
